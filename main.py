from db import dbConfiguration

cursor = dbConfiguration.connection.cursor()
connection = dbConfiguration.connection

cursor.execute("USE students_db")

stmt = "SELECT student_id, student_name, student_surname, birth FROM students WHERE student_id = (SELECT student_id FROM students order by birth asc LIMIT 1) UNION SELECT student_id, student_name, student_surname, birth FROM students WHERE student_id = (SELECT student_id FROM students order by birth desc LIMIT 1)"
cursor.execute(stmt)

students = []

f = open("artifacts/result.txt", "w")
for (student_id, student_name, student_surname, birth) in cursor:
    students.append(student_name)
    students.append(student_surname)
    students.append(birth)
    kek = student_name, student_surname, birth

    f.write(student_name + ' ' + student_surname + ' ' + f"{birth}" + '\n')
    print(f"{student_id}, {student_name}, {student_surname}, {birth}")

f.close()
