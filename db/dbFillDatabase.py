import dbConfiguration

cursor = dbConfiguration.connection.cursor()
connection = dbConfiguration.connection
# Создание бд
cursor.execute("CREATE DATABASE students_db")
cursor.execute("USE students_db")

# Создание таблиц
cursor.execute(
    "create table students(student_id int auto_increment, student_name varchar(255) not null,student_surname varchar(255) not null, birth date, primary key(student_id))")
cursor.execute(
    "create table info(id int auto_increment, subject_name varchar(255) not null,exam_date date , grade int, proff_name varchar(255), student_id int, PRIMARY KEY (id), FOREIGN KEY (student_id) REFERENCES students(student_id));")
cursor.execute("INSERT INTO students VALUES (1, 'Jony', 'Sheleg', '2002-02-09');")
cursor.execute("INSERT INTO students VALUES (2, 'Ivan', 'Popov', '2001-05-11');")
cursor.execute("INSERT INTO students VALUES (3, 'Henry', 'Chaplin', '2002-08-17');")
# оценки студента 1
cursor.execute("INSERT INTO info VALUES (1, 'Programming', '2022-12-21', 5, 'Pavel Andreevich', 1);")
cursor.execute("INSERT INTO info VALUES (2, 'History', '2022-12-22', 3, 'Kirill Alekseevich', 1);")
cursor.execute("INSERT INTO info VALUES (3, 'Art', '2022-12-23', 5, 'Elena Maksimovna', 1);")
# оценки студента 2
cursor.execute("INSERT INTO info VALUES (4, 'Programming', '2022-12-20', 4, 'Pavel Andreevich', 2);")
cursor.execute("INSERT INTO info VALUES (5, 'History', '2022-12-22', 4, 'Kirill Alekseevich', 2);")
cursor.execute("INSERT INTO info VALUES (6, 'Art', '2022-12-20', 4, 'Elena Maksimovna', 2);")
# оценки студента 3
cursor.execute("INSERT INTO info VALUES (7, 'History', '2022-12-20', 5, 'Kirill Alekseevich', 3);")
cursor.execute("INSERT INTO info VALUES (8, 'Programming', '2022-12-20', 5, 'Pavel Andreevich', 3);")
cursor.execute("INSERT INTO info VALUES (9, 'Art', '2022-12-20', 5, 'Elena Maksimovna', 3);")
connection.commit()
