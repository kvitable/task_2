import os
import mariadb
import sys

try:
    connection = mariadb.connect(
        user='root',
        password=os.environ['MARIA_DB_PASSWORD'],
        host=os.environ['MARIA_DB_HOST'],
        port=os.environ['MARIA_DB_PORT'],
    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)
