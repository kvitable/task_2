FROM alpine:latest
RUN apk add --update python3 py-pip
RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add --no-cache mariadb-dev
RUN python3 -m pip install --upgrade pip
RUN pip3 install mariadb==1.1.5
COPY ./ ./workdir
CMD [ "python3", "./workdir/db/dbConfiguration.py"]
CMD [ "python3", "./workdir/db/dbFillDatabase.py"]
CMD [ "python3", "./workdir/main.py"]
